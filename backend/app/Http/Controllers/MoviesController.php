<?php

namespace App\Http\Controllers;

use App\Movies;

use Illuminate\Http\Request;

class MoviesController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMovie(Request  $request)
    {
        $movie = new Movies();
        $movie->title = $request->titulo;
        $movie->year = $request->anio;
        $movie->image = $request->imagen;
        $movie->description = $request->descripcion ;
        $movie->save();

        // if ($this->loginAfterSignUp) {
        //     return  $this->login($request);
        // }

        return  response()->json([
            'status' => 'ok',
            'data' => $movie
        ], 200);
    }

    public function getMovies(Request  $request)
    {
        $movie = new Movies();
        $data=$movie->getMovies();
        
        return  response()->json([
            'status' => 'ok',
            'data' => $data
        ], 200);
    }

    // public  function  login(Request  $request) {
    //     $input = $request->only('email', 'password');
    //     $jwt_token = null;
    //     if (!$jwt_token = JWTAuth::attempt($input)) {
    //         return  response()->json([
    //             'status' => 'invalid_credentials',
    //             'message' => 'Correo o contraseña no válidos.',
    //         ], 401);
    //     }

    //     return  response()->json([
    //         'status' => 'ok',
    //         'token' => $jwt_token,
    //     ]);
      
    // }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //metodo para crear una pelicula
    public function store(Request $request)
    {
       if($request->hasFile('image')){
           $file = $request->file('image');
           $name = time().$file->getClientOriginalName();
           $file->move(public_path().'/images/',$name) ;
       }
       $movie = new Movies();
       $movie->title = $request->titulo;
       $movie->year = $request->anio;
       $movie->description = $request->descripcion ;
       $movie->save();
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libros=Libro::find($id);
        return  view('libro.show',compact('libros'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $libro=libro::find($id);
        return view('libro.edit',compact('libro'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {

    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
}
}