<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class Movies extends Model
{
    use Notifiable;
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'year', 'description', 'image',
    ];

    /*       $table->string('title');
            $table->string('year');
            $table->string('description');
            $table->string('image') ;  */
    public function getMovies()
    {        
    $movies = DB::table('movies')->get()->toArray();        
        return $movies;        
    }
}
