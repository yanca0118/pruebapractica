import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { css } from '@emotion/core';
import Router from 'next/router';
import Layout from '../components/layout/Layout';
import { Form, InputRegister, InputSubmit,Error } from '../components/ui/Form';
import {LogUserAction} from '../components/actions/userAction';
import Header from '../components/layout/Header';





const Login = () => {


	const dispatch = useDispatch();
	const [email, saveEmail] = useState('');
	const [password, savePassword] = useState('');
	// Acceder al state del store
    const load = useSelector( state => state.data.loading );
	const error = useSelector(state => state.data.error);
	const info = useSelector(state => state.data);
	//const alerta = useSelector(state => state.alerta.alerta);
	const login = data => dispatch( LogUserAction(data) );

	
	const submit = (e) => {
		e.preventDefault();

		// validar formulario
		if (email.trim() === '' || password.trim() === '') {
			console.log('campos vacios');
		
			return;
		}


		// envia la información del usuario
		login({
			email,
			password
		});

		// redireccionar
		//Router.push('/');
	};

	return (
		<div>
			<Layout>
				<h1
					css={css`
						text-align: center;
						margin-top: 5rem;
					`}>
					Iniciar Sesión
				</h1>
				<Form onSubmit={submit}>

					<InputRegister>
						<label htmlFor="email">Email</label>
						<input
							type="email"
							id="email"
							placeholder="Tu Email"
							name="email"
							value={email}
							onChange={(e) => saveEmail(e.target.value)}
						/>
					</InputRegister>

					{/* {errores.email && <Error>{errores.email}</Error>} */}

					<InputRegister>
						<label htmlFor="password">Password</label>
						<input
							type="password"
							id="password"
							placeholder="Tu password"
							name="password"
							value={password}
							onChange={(e) => savePassword(e.target.value)}
						/>
					</InputRegister>

					{/* {errores.password && <Error>{errores.password}</Error>}
					{error && <Error>{error}</Error>} */}
					<InputSubmit type="submit" value="Iniciar Sesión" />
					{error && <Error>{error}</Error>}
				</Form>
				
				{ load ? <p>Cargando...</p> : null }
                        
                { error ? <p className="alert alert-danger p2 mt-4 text-center">Hubo un error</p> : null }
			</Layout>
		</div>
	);
};

export default Login;
