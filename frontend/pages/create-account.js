import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { css } from '@emotion/core';
import Router from 'next/router';
import Layout from '../components/layout/Layout';
import { Form, InputRegister, InputSubmit, Error } from '../components/ui/Form';

import {createNewUserAction} from '../components/actions/userAction';


//validaciones
//mport useValidacion from '../hooks/useValidacion';
//import validarCrearCuenta from '../validacion/validarCrearCuenta';

const Createaccount = () => {
	const [name, saveName] = useState('');
	const [email, saveEmail] = useState('');
	const [password, savePassword] = useState('');

	// utilizar use dispatch y te crea una función
	//const dispatch = useDispatch();
	const dispatch = useDispatch();
	// Acceder al state del store
	//const cargando = useSelector( state => state.productos.loading );
	//const error = useSelector(state => state.productos.error);
	//const alerta = useSelector(state => state.alerta.alerta);

	const addUser = data => dispatch( createNewUserAction(data) );
	// cuando el usuario haga submit
	const submit = (e) => {
		e.preventDefault();

		// validar formulario
		if (name.trim() === '' || email.trim() === '' || password.trim() === '') {
			console.log('campos vacios');
			// const alerta = {
			//     msg: 'Ambos campos son obligatorios',
			//     classes: 'alert alert-danger text-center text-uppercase p3'
			// }
			// dispatch( mostrarAlerta(alerta) );

			return;
		}

		// si no hay errores
		// dispatch( ocultarAlertaAction() );

		// crear el nuevo usuario
		addUser({
			name,
			email,
			password
		});

		// redireccionar
		Router.push('/');
	};

	return (
		<div>
			<Layout>
				<h1
					css={css`
						text-align: center;
						margin-top: 5rem;
					`}>
					Crear Cuenta
				</h1>
				<Form onSubmit={submit}>
					<InputRegister>
						<label htmlFor="nombre">Nombre</label>
						<input
							type="text"
							id="nombre"
							placeholder="Tu nombre"
							name="nombre"
							value={name}
							onChange={(e) => saveName(e.target.value)}
						/>
					</InputRegister>

					{/* {errores.nombre && <Error>{errores.nombre}</Error>} */}

					<InputRegister>
						<label htmlFor="email">Email</label>
						<input
							type="email"
							id="email"
							placeholder="Tu Email"
							name="email"
							value={email}
							onChange={(e) => saveEmail(e.target.value)}
						/>
					</InputRegister>

					{/* {errores.email && <Error>{errores.email}</Error>} */}

					<InputRegister>
						<label htmlFor="password">Password</label>
						<input
							type="password"
							id="password"
							placeholder="Tu password"
							name="password"
							value={password}
							onChange={(e) => savePassword(e.target.value)}
						/>
					</InputRegister>
					
					{/* {errores.password && <Error>{errores.password}</Error>}
	                {error && <Error>{error}</Error>} */}
					<InputSubmit type="submit" value="Crear Cuenta" />
					
				</Form>
			</Layout>
		</div>
	);
};
export default Createaccount;
