import React,{useEffect,useState , useContext } from 'react';
import Layout from '../components/layout/Layout';
// import DetallesProducto from '../components/layout/DetallesProducto';
// import useProductos from '../hooks/useProducto' ;
import Movies from '../components/layout/Movies';


const Favorite = () => {

	// const {productos} = useProductos('votos');
	const data = [
	
		{
		  id:1,
		  title: 'Titanic',
		  image: 'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80',
		  year: '2020' ,
		  description: 'por incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat'
  
		},
		{
		  id:3 ,
		  title: 'Advengers',
		  image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
		  year: '2020' ,
		  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat'
  
		},
		{
		  id:4,
		  title: 'Dragon ball z',
		  image: 'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80',
		  year: '2020' ,
		  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat'
  
		}
	  ]

	return (
		<div>
			<Layout>
				<div className="movie-list">
					<div className="container">
						<ul className="bg-white">
						{data.map(movie => (
								<Movies
								key={movie.id}
							    movie={movie}
							/>
							))}
							 
						</ul>
					</div>
				</div>
			</Layout>
		</div>
	);
};

export default Favorite;
