import React, { useState, useContext } from 'react';
import { css } from '@emotion/core';
import { useDispatch, useSelector } from 'react-redux';
import Router, { useRouter } from 'next/router';
import Layout from '../components/layout/Layout';
import { Form, InputRegister, InputSubmit,Error } from '../components/ui/Form';
import FileUploader from 'react-firebase-file-uploader';
import Error404 from '../components/layout/404';

import {createMovieAction} from '../components/actions/movieAction';


//validaciones
// import useValidacion from '../hooks/useValidacion';
// import validarCrearProducto from '../validacion/validarCrearProducto';

// const STATE_INICIAL = {
// 	nombre: '',
// 	empresa: '',
// 	url: '',
// 	descripcion: ''
// };
//imagen: '' ,

const AddMovie = () => {

	const dispatch = useDispatch();

	const usuario = true ;

	const [titulo, saveTitulo] = useState('');
	const [anio, saveAnio] = useState('');
	const [descripcion, saveDescripcion] = useState('');
	const [imagen, saveImagen] = useState('');

	const addMovie = data => dispatch( createMovieAction(data) );


	const submit = (e) => {
		e.preventDefault();

		// validar formulario
		if (titulo.trim() === '' || anio.trim() === '' || descripcion.trim() === '' || imagen.trim() === '') 
		{
			console.log('campos vacios');
			return;
		}

		// crear el nuevo usuario
		addMovie({
			titulo,
			anio,
			descripcion,
			imagen
		});

		// redireccionar
		Router.push('/');
	};

	return (
		<div>
			<Layout>
				{!usuario ? (
					<Error404 />
				) : (
					<>
						<h1
							css={css`
								text-align: center;
								margin-top: 5rem;
							`}>
							Nueva Película
						</h1>
						<Form onSubmit={submit}>
							<fieldset>
								<legend>Información General</legend>
								<InputRegister>
									<label htmlFor="title">Título</label>
									<input
										type="text"
										id="title"
										placeholder="Título"
										name="title"
										value={titulo}
										onChange={(e) => saveTitulo(e.target.value)}
									/>
								</InputRegister>

								{/* {errores.nombre && <Error>{errores.nombre}</Error>} */}

								<InputRegister>
									<label htmlFor="year">Año</label>
									<input
										type="text"
										id="year"
										placeholder="Escriba el año de estreno"
										name="year"
										value={anio}
										onChange={(e) => saveAnio(e.target.value)}
									/>
								</InputRegister>

								{/* {errores.empresa && <Error>{errores.empresa}</Error>} */}

								<InputRegister>
									<label htmlFor="imagen">Imagen</label>
									<FileUploader
										accept="image/*"
										id="imagen"
										name="imagen"
										randomizeFilename
										value={imagen}
										onChange={(e) => saveImagen(e.target.value)}
									/>
						
								</InputRegister>
								{/* <InputRegister>
									<label htmlFor="url">URL</label>
									<input
										type="url"
										id="url"
										name="url"
										placeholder="URL de tu producto"
									
									/>
								</InputRegister> */}
								{/* {errores.url && <Error>{errores.url}</Error>} */}
							</fieldset>
							<fieldset>
								<legend>Sobre la Película</legend>

								<InputRegister>
									<label htmlFor="descripcion">Descripcion</label>
									<textarea
										id="descripcion"
										name="descripcion"
										value={descripcion}
										onChange={(e) => saveDescripcion(e.target.value)}
									/>
								</InputRegister>
								{/* {errores.descripcion && <Error>{errores.descripcion}</Error>} */}
							</fieldset>

							{/* {error && <Error>{error}</Error>} */}
							<InputSubmit type="submit" value="Guardar Película" />
						</Form>
					</>
				)}
			</Layout>
		</div>
	);
};

export default AddMovie;
