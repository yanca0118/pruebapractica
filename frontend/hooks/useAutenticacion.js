import React, {useEffect , useState} from 'react' ; 
import firebase from '../firebase' ; 

function useAutenticacion () { 
    const [usuarioAutenticado , guardarUsuarioAutenticado ] = useState(null);
    useEffect(() => { 
        const unsuscribe = firebase.auth.onAuthStateChanged(usuario => {
            //revisa si hay un usuario autenticado y lo guarda en el state
            if(usuario){
                guardarUsuarioAutenticado(usuario); 
            }else {
                guardarUsuarioAutenticado(null);
            }
        });
        return () => unsuscribe();
    },[]);
    return usuarioAutenticado ; 
}
export default useAutenticacion ; 