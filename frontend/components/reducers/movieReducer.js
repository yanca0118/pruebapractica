import {
    SAVE_MOVIE,
    SAVE_MOVIE_SUCCESS,
    SAVE_MOVIE_ERROR,
    LOAD_IMAGE,
    LOAD_IMAGE_SUCCESS,
    LOAD_IMAGE_ERROR
} from '../types';

//cada reducer tiene su propio state 
const initialState = {
    data: [],
    error: false,
    loading: false
}

export default function (state = initialState, action) {
    switch (action.type) {

        case SAVE_MOVIE:
            return {
                ...state,
                error: null,
                loading: true
            }

        case SAVE_MOVIE_SUCCESS:

            return {
                ...state,
                loading: false,
                data: [...state.data, action.payload],
                error:null
            }

        case SAVE_MOVIE_ERROR:

            return {
                ...state,
                loading: false,
                error: action.payload
            }
        
            case LOAD_IMAGE:
                return {
                    ...state,
                    error: null,
                    loading: true
                }
    
            case LOAD_IMAGE_SUCCESS:
    
                return {
                    ...state,
                    loading: false,
                    data: [action.payload],
                    error:null
                }
    
            case LOAD_IMAGE_ERROR:
    
                return {
                    ...state,
                    loading: false,
                    error: action.payload
                }

        default:
            return state;
    }
}