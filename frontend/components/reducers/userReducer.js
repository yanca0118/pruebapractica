import {
    REGISTER_USER , 
    REGISTER_USER_SUCESS , 
    REGISTER_USER_ERROR , 
    LOGIN_USER , 
    LOGIN_USER_SUCESS , 
    LOGIN_USER_ERROR
} from '../types' ;

//cada reducer tiene su propio state 
const initialState = { 
    data : [] , 
    error : null ,
    loading:false
}

export default function(state = initialState,action) { 
    switch(action.type) { 

        case REGISTER_USER : 
             return { 
                 ...state , 
                 loading : action.payload 
             }
        
        case REGISTER_USER_SUCESS : 
        
            return { 
                ...state , 
                loading : false , 
                data : [...state.data,action.payload]
            }

        case REGISTER_USER_ERROR :  
            
            return { 
                ...state , 
                loading:false, 
                error:action.payload
            }

        case LOGIN_USER : 
            return { 
                ...state , 
                loading : action.payload 
            }

        case LOGIN_USER_ERROR : 
        
            return { 
                ...state , 
                loading : false , 
                data : [...state.data,action.payload]
            }    

        case LOGIN_USER_ERROR : 
            return { 
                ...state , 
                loading:false, 
                error:action.payload
            }
           
           

        default:
            return state ; 
    }
}