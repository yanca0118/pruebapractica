import React, { useContext } from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import Search from '../ui/Search';
import Navigation from './Navigation';
import Boton from '../ui/Boton';

const ContainerHeader = styled.div`
	max-width: 1200px;
	width: 95%;
	margin: 0 auto;
	@media (min-width: 768px) {
		display: flex;
		justify-content: space-between;
	}
`;

const Logo = styled.a`
	color: var(--naranja);
	font-size: 4rem;
	line-height: 0;
	font-weight: 700;
	font-family: 'Roboto Slab', serif;
	margin-right: 2rem;
`;

const Header = ({info}) => {

    const usuario = true ;
    
	return (
		<header
			css={css`
				border-bottom: 2px solid var(--gris3);
				padding: 1rem 0;
			`}>
			<ContainerHeader>
				<div
					css={css`
						display: flex;
						align-items: center;
					`}>
					<Link href="/">
						<Logo>Pelisplus</Logo>
					</Link>

					<Search />

					<Navigation />
				</div>

				<div
					css={css`
						display: flex;
						align-items: center;
					`}>
				
					{ usuario ? (
                        <>
                            <p
                                css={css`
                                    margin-right: 2rem;
                                `}
                            >Hola: Juan</p>
                            <Boton
                                bgColor="true"
                                onClick={() => firebase.cerrarSesion()}
                            >Cerrar Sesión</Boton>
                        </>
                    ) : (
                        <>
                            <Link href="/login">
                                <Boton
                                    bgColor="true"
                                >Login</Boton>
                            </Link>
                            <Link href="/create-account">
                                <Boton>Crear Cuenta</Boton>
                            </Link>
                        </>
                    ) }
				</div>
			</ContainerHeader>
		</header>
	);
};

export default Header;
