import React, { useContext } from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';

const Nav = styled.nav`
	padding-left: 2rem;
	a {
		font-size: 1.8rem;
		margin-left: 2rem;
		color: var(--gris2);
		font-family: 'PT Sans', sans-serif;

		&:last-of-type {
			margin-right: 0;
		}
	}
`;

const Navegacion = () => {
	const usuario = true;

	return (
		<Nav>
			{usuario ? (
				<>
					<Link href="/">
						<a>Inicio</a>
					</Link>
					<Link href="/favorite">
						<a>Favoritas</a>
					</Link>
					<Link href="/new-movie">
						<a>Nueva Pelicula</a>
					</Link>
				</>
			) : (
				<>
					<Link href="/">
						<a>Inicio</a>
					</Link>
				</>
			)}
		</Nav>
	);
};

export default Navegacion;
