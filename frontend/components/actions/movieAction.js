//importación de types
import {
    SAVE_MOVIE,
    SAVE_MOVIE_SUCCESS,
    SAVE_MOVIE_ERROR,
    LOAD_IMAGE,
    LOAD_IMAGE_SUCCESS,
    LOAD_IMAGE_ERROR
} from '../types';
import axios from 'axios';
import Router from 'next/router';

import clientAxios from '../../config/axios' ; 
//crea un nuevo usuario
export function createMovieAction(data) {
	return async (dispatch) => {
		dispatch(startSaveMovie());
    debugger
        console.log(data);
		try {
			//se inserta la información del usuario en la base de datos
			 await clientAxios.post('/registerMovies', data);
			// si todo sale bien , actualiza el state
		
			dispatch(SaveMovieSuccess(data));
			
		} catch (error) {
            dispatch(SaveMovieError(data)); 
			//console.log(error.response.data.errores[0].msg);
			console.log(error) ;
		}
	};
}

export function getMoviesAction(){

    return async (dispatch)=>{

        dispatch(loadImages());
        
        try {
            
            var data= await clientAxios.get('/getMovies');    
            dispatch(loadImagesSuccess(data.data.data)); 

        } catch (error) {
             dispatch(loadImagesError(data)); 
        }

    }
}


const startSaveMovie = () => ({
	type: SAVE_MOVIE,
});

//si el usuario se guarda en la base de datos
const SaveMovieSuccess = (data) => ({
	type: SAVE_MOVIE_SUCCESS,
	payload: data
});

//si ocurre un error a la hora de registrar
const SaveMovieError = (error) => ({
	type: SAVE_MOVIE_ERROR,
	payload: error
});

const loadImages = () => ({
	type: LOAD_IMAGE,
});

//si el usuario se guarda en la base de datos
const loadImagesSuccess = (data) => ({
	type: LOAD_IMAGE_SUCCESS,
	payload: data
});

//si ocurre un error a la hora de registrar
const loadImagesError = (error) => ({
	type: LOAD_IMAGE_ERROR,
	payload: error
});


