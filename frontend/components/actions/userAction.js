//importación de types
import { REGISTER_USER, REGISTER_USER_SUCESS, REGISTER_USER_ERROR ,LOGIN_USER,LOGIN_USER_SUCESS,LOGIN_USER_ERROR} from '../types'
import axios from 'axios';
import Router from 'next/router';

import clientAxios from '../../config/axios' ; 
//crea un nuevo usuario
export function createNewUserAction(data) {
	return async (dispatch) => {
		dispatch(registerUSer());
		console.log(data);
		try {
			//se inserta la información del usuario en la base de datos
			 await clientAxios.post('/register', data);
			// si todo sale bien , actualiza el state
			/*
			axios({
				method: 'post',
				url: 'http://localhost:8000/api/register',
				data: data,
				config: { headers: {'Content-Type': 'multipart/form-data' }}
				})
				.then(function (response) {
					//handle success
					console.log(response);
				})
				.catch(function (response) {
					//handle error
					console.log(response);
				});
			*/	
			
			dispatch(registerUSerSucess(data));
			
		} catch (error) {
			//console.log(error.response.data.errores[0].msg);
			console.log(error) ;
			// let errorMsg = {};
			// error.response.data.msg == undefined
			// 	? (errorMsg.msg = error.response.data.errores[0].msg)
			// 	: (errorMsg.msg = error.response.data.msg);
			// // si hay un error cambia el state
			// // dispatch(registerUSerError(errorMsg)) ;
			// toastRef.current.show(errorMsg.msg, 2500);
		}
	};
}

export function LogUserAction(data) {
	return async (dispatch) => {
		dispatch(loginUser());
		console.log(data);
		try {
			//se inserta la información del usuario en la base de datos
			 await clientAxios.post('/login', data);
			// console.log(test);
			// si todo sale bien , actualiza el state
			dispatch(loginUserSucess(data));
			Router.push('/');
			
		} catch (error) {
			//console.error('Hubo un error al autenticar el usuario', error.message);
			dispatch( loginUSerError(true) );
		}
	};
}


const registerUSer = () => ({
	type: REGISTER_USER,
	payload: true
});

//si el usuario se guarda en la base de datos
const registerUSerSucess = (data) => ({
	type: REGISTER_USER_SUCESS,
	payload: data
});

//si ocurre un error a la hora de registrar
const registerUSerError = (error) => ({
	type: REGISTER_USER_ERROR,
	payload: error
});



const loginUser = () => ({
	type:LOGIN_USER , 
	payload : true 
});

const loginUserSucess = (data) => ({
	type:LOGIN_USER_SUCESS , 
	payload : data 
});


const loginUSerError = (error) => ({
	type: LOGIN_USER_ERROR,
	payload: error
});

