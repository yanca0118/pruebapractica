export default function validateCreateAccount(value){
    let error = {} ; 
    //validar el nombre del usuario 
    if(!value.nombre) {
        error.nombre = "El Nombre es obligatorio" ;
    }
    //validar el email 
    if(!value.email){
        error.email ="El Email es obligatorio" ;
    }else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value.email)){
        error.email = "Email no válido" ;
    }

    // validar el password 
    if(!value.password) {
        error.password ="El password es obligatorio" ;
    }else if(value.password.length < 6) {
        //en firebase una contraseña debe de tener al menos 6 digitos
        error.password = 'El password debe ser de al menos 6 caracteres'
    }

    return error ; 
}