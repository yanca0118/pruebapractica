import axios from 'axios';

const clienteAxios = axios.create({
    baseURL: 'https://fathomless-wave-17736.herokuapp.com/'
});

export default clienteAxios;